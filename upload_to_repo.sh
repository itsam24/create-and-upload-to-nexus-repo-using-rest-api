#! /bin/bash

upload from one instance to nexus located instance without rest api

curl -v -u admin:admin123 --upload-file java/jdk1.8.0_131.tar.gz http://52.40.136.224:8081/nexus/content/repositories/releases/java_1.8.tar.gz


upload from one instance to nexus located instance using rest api

curl -v -F r=releases -F hasPom=false -F file=@jdk1.8.0_131.tar.gz -u admin:admin123 http://52.40.136.224:8081/nexus/content/repositories/releases/java_1.8.1.tar.gz